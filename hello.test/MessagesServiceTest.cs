using hello.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace hello.test
{
    [TestClass]
    public class MessagesServiceTest
    {
        [TestMethod]
        public void TestGenerateDescriptionMethod()
        {
            var service = new MessagesService();

            Assert.AreEqual("Your application description page.", service.GenerateDescription());
        }

        [TestMethod]
        public void TestGenerateContactMethod()
        {
            var service = new MessagesService();

            Assert.AreEqual("Your contact page.", service.GenerateContact());
        }
    }
}
