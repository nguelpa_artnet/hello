﻿namespace hello.Services
{
    public class MessagesService
    {
        public string GenerateDescription()
        {
            return "Your application description page.";
        }

        public string GenerateContact()
        {
            return "Your contact page.";
        }
    }
}
