﻿using hello.Services;
using Microsoft.AspNetCore.Mvc;

namespace hello.Controllers
{
    public class HomeController : Controller
    {
        private readonly MessagesService _service;

        public HomeController(MessagesService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = _service.GenerateDescription();

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = _service.GenerateContact();

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
